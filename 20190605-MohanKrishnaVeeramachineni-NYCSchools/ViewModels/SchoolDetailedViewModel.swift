//
//  SchoolDetailedViewModel.swift
//  20190605-MohanKrishnaVeeramachineni-NYCSchools
//
//  Created by Mohan Krishna  on 6/6/19.
//  Copyright © 2019 Mohan Krishna . All rights reserved.
//

import Foundation

//School Detailed  View Model
struct SchoolDetailedViewModel {
    
    private let schoolDetails:SchoolDetails
    var overView:String {
        if let overView = UserDefaults.standard.string(forKey: Constants.overviewParagraph)
        {
            return overView
        }else {return "No School Overview Found in the API"}
    }

    var schoolName:String {
        return self.schoolDetails.school_name
    }
    var numberOfSatTestTakers:String {
        return self.schoolDetails.num_of_sat_test_takers
    }
    
    var satReadingAverageScores:String {
        return self.schoolDetails.sat_critical_reading_avg_score
    }
    var satMathAverageScores:String {
        return self.schoolDetails.sat_math_avg_score
    }
    
    var satWritingAverageScores:String {
        return self.schoolDetails.sat_writing_avg_score
    }
    
}

extension SchoolDetailedViewModel{
    init(_ schoolDetails: SchoolDetails) {
        self.schoolDetails = schoolDetails
    }
}

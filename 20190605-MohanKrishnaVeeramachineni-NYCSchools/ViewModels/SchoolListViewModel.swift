//
//  SchoolListViewModel.swift
//  20190605-MohanKrishnaVeeramachineni-NYCSchools
//
//  Created by Mohan Krishna  on 6/6/19.
//  Copyright © 2019 Mohan Krishna . All rights reserved.
//

import Foundation
import UIKit

//School List View Model
struct SchoolListViewModel {
    
     let schools:[School]
    
    var numberOfSections:Int {
        return 1
    }
    
    func numberOfRowsInSection(_ section: Int) -> Int {
        return self.schools.count
    }
    
    func schoolAtIndex(_ index:Int) -> SchoolViewModel{
        let school = self.schools[index]
        return SchoolViewModel(school)
    }

}


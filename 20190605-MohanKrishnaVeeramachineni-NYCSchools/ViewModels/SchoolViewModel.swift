//
//  schoolViewModel.swift
//  20190605-MohanKrishnaVeeramachineni-NYCSchools
//
//  Created by Mohan Krishna  on 6/6/19.
//  Copyright © 2019 Mohan Krishna . All rights reserved.
//

import Foundation
import UIKit

//School View Model
struct SchoolViewModel {
    
    private let school:School
    
    //School - name
    var name:String {
        return self.school.school_name
    }
    // School - location
    var location:String {
      return self.school.primary_address_line_1 + " " + self.school.city + " " + self.school.state_code + " " + self.school.zip
    }
    //School - dbn
    var dataBaseName:String {
        return self.school.dbn
    }
    
    //School - overViewParagraph
    var overViewParagraph:String{
        return self.school.overview_paragraph
    }
}


extension SchoolViewModel{
    init(_ school: School) {
        self.school = school
    }
}



//
//  schoolViewCell.swift
//  20190605-MohanKrishnaVeeramachineni-NYCSchools
//
//  Created by Mohan Krishna  on 6/6/19.
//  Copyright © 2019 Mohan Krishna . All rights reserved.
//

import Foundation
import UIKit

//School Table view cell
class SchoolTableViewCell:UITableViewCell{
    @IBOutlet weak var schoolName: UILabel!
    @IBOutlet weak var locationName:UILabel!
}

//Constants
struct Constants {
    static let schoolListUrl = "https://data.cityofnewyork.us/resource/s3k6-pzi2"
    static let schoolDetailUrl = "https://data.cityofnewyork.us/resource/f9bf-2cp4.json"
    static let dataBaseName = "dbn"
    static let overviewParagraph = "overview_paragraph"
}

//
//  School.swift
//  20190605-MohanKrishnaVeeramachineni-NYCSchools
//
//  Created by Mohan Krishna  on 6/6/19.
//  Copyright © 2019 Mohan Krishna . All rights reserved.
//

import Foundation

//School model
struct School:Decodable {
    let dbn:String
    let school_name:String
    let location:String
    let phone_number:String
    let primary_address_line_1:String
    let city:String
    let zip:String
    let state_code:String
    let website:String
    let overview_paragraph:String
}



//
//  SchoolDetail.swift
//  20190605-MohanKrishnaVeeramachineni-NYCSchools
//
//  Created by Mohan Krishna  on 6/6/19.
//  Copyright © 2019 Mohan Krishna . All rights reserved.
//

import Foundation

//SchoolDetails model
struct SchoolDetails:Decodable {
    let dbn:String
    let school_name:String
    let num_of_sat_test_takers:String
    let sat_critical_reading_avg_score:String
    let sat_math_avg_score:String
    let sat_writing_avg_score:String
}

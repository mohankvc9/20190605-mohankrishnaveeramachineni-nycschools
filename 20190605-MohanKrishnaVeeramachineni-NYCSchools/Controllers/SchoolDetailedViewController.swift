//
//  SchoolDetailedViewController.swift
//  20190605-MohanKrishnaVeeramachineni-NYCSchools
//
//  Created by Mohan Krishna  on 6/6/19.
//  Copyright © 2019 Mohan Krishna . All rights reserved.
//

import Foundation
import UIKit
class SchoolDetailedViewController: UIViewController {
    
    @IBOutlet weak var schoolOverViewTextView: UITextView!
    
    @IBOutlet weak var satWritingAverageValue: UILabel!
    
    @IBOutlet weak var satMathAverageValue: UILabel!
    
    @IBOutlet weak var satReadingAverageValue: UILabel!
    
    private var schoolDetailsVM:SchoolDetailedViewModel!
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
    }
    
    private func setup() {
        self.navigationController?.navigationBar.prefersLargeTitles = true
        let url = URL(string:Constants.schoolDetailUrl)!
        // Rest-Api Call --> gets array of schoolDetailModels.
        WebService<SchoolDetails>().getSchoolData(url: url) { (schoolDetails) in
            if let schoolDetails = schoolDetails{
                
                guard let dataBaseName = UserDefaults.standard.string(forKey: Constants.dataBaseName) else {return}
                // filtering the required schoolDetailsModel from the array of School Details retreived from rest api call
                guard let requiredSchoolDetails = schoolDetails.first(where: {$0.dbn == dataBaseName}) else {return}
                // populating the schoolDetailsVM from the filtered result
                self.schoolDetailsVM = SchoolDetailedViewModel(requiredSchoolDetails)
                DispatchQueue.main.async {
                    self.loadUI()
                }
            }
        }
    }
    //Populating the UI
    private func loadUI(){
        schoolOverViewTextView.text = self.schoolDetailsVM.overView
        satWritingAverageValue.text = self.schoolDetailsVM.satWritingAverageScores
        satMathAverageValue.text = self.schoolDetailsVM.satMathAverageScores
        satReadingAverageValue.text = self.schoolDetailsVM.satReadingAverageScores
    }
    //Done Btn clicked. going back to the school list View controller
    @IBAction func doneBtnClicked(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
}

//
//  schoolListViewController.swift
//  20190605-MohanKrishnaVeeramachineni-NYCSchools
//
//  Created by Mohan Krishna  on 6/6/19.
//  Copyright © 2019 Mohan Krishna . All rights reserved.
//

import Foundation
import UIKit

class SchoolListViewController: UITableViewController {
    
    private var schoolListVM: SchoolListViewModel!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
    }
    //Making a WebService Call
    private func setup() {
      self.navigationController?.navigationBar.prefersLargeTitles = true
        //creating url variable. Here the url string(static value) is stored in the Constants class
        let url = URL(string:Constants.schoolListUrl)!
        WebService<School>().getSchoolData(url: url) { schools in
            if let schools = schools{
                //populating the SchoolListViewModel from SchoolViewModel
                self.schoolListVM = SchoolListViewModel(schools: schools)
                DispatchQueue.main.async {
                    //Updating the UI
                    self.tableView.reloadData()
                }
            }
        }
    }
    
    // tableView numberOfSections delegate function --> populating the value from the ViewModel
    override func numberOfSections(in tableView: UITableView) -> Int {
        return self.schoolListVM == nil ? 0 : self.schoolListVM.numberOfSections
    }
    
    //tableView numberOfRowsInSection delegatefunction --> populating the value from the viewModel
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.schoolListVM.numberOfRowsInSection(section)
    }
    //tableview cellForRowAt delegate function
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "SchoolTableViewCell") as? SchoolTableViewCell else {
            fatalError("School table view cell not found")
        }
        cell.locationName.text = self.schoolListVM.schoolAtIndex(indexPath.row).location
        cell.schoolName.text = self.schoolListVM.schoolAtIndex(indexPath.row).name
        return cell
    }
    
    // tableView didSelectRowAt delegate function
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        // getting the dbName and the overView Paragraph name from the ViewModel
        let schoolData = self.schoolListVM.schoolAtIndex(indexPath.row)
        let dataBaseName = schoolData.dataBaseName
        let overViewParagraph = schoolData.overViewParagraph
        //Storing the dbName and overViewParagraph values in the userdefaults
        UserDefaults.standard.set(dataBaseName, forKey: Constants.dataBaseName)
        UserDefaults.standard.set(overViewParagraph, forKey: Constants.overviewParagraph)
        // Go to the detailed page
        self.performSegue(withIdentifier: "showDetail", sender: self)
    }

}

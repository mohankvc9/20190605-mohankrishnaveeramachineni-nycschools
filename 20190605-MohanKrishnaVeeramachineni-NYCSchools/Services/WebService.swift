//
//  webService.swift
//  20190605-MohanKrishnaVeeramachineni-NYCSchools
//
//  Created by Mohan Krishna  on 6/6/19.
//  Copyright © 2019 Mohan Krishna . All rights reserved.
//

import Foundation

//WebService class used to make a network call and get the data. It is a generic to support two types of Api-Model decoding operation!!
public class WebService<T:Decodable> {

    func getSchoolData(url:URL, completion:@escaping([T]?) -> ()){
    
        URLSession.shared.dataTask(with: url) { (data, response, error) in
            
            if let error = error {
                print(error.localizedDescription)
            } else if let data = data {
                if let schoolList =  try? JSONDecoder().decode([T].self, from: data){
                    completion(schoolList)
                }
            }
            
        }.resume()
    }
    
}
